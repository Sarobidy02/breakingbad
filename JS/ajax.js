const api = "https://www.breakingbadapi.com/api/";

button = $(".quote");
place_quote = $(".place_quote");
recherche = $("#recherche");


function creat_col_of_card(data){
    result = `<div class="col">
                <div class="card" style="width: 18rem;">
                    <img class="card-img-top character_image" src="${data.img}" alt="Card image cap">
                    <div class="card-body">
                        <p id="${data.id}" class="card-text name">${data.name}</p>
                    </div>
                </div>
            </div>`;
    return result;
}

//injecting the json data as a child of element_to_insert
function insertHtmlCharacter(data, element_to_insert){
    //remove the previous valeu inside
    $(element_to_insert).empty();
    items = 0;
    len_data = data.length;
    while(items<len_data){
        //create a new row
        $(element_to_insert).append(`<div class="row ligne"></div>`);
        last_row = $(element_to_insert).children().last();
        colon = 0;
        //creating colum
        while(colon<4 && items<len_data){
            last_row.append(creat_col_of_card(data[items]));
            colon++;
            items++;
        }
    }
};
//request 16 characters form index offset
function load_data(offset){
    fetch(api+`characters?limit=16&offset=${offset}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        },
    }).then(reponse => reponse.json()).then(data => insertHtmlCharacter(data, "#contenue"));
    
}
function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}
function insertQuote(data){
    text = `<blockquote class="blockquote text-center">
    <p class="mb-0">${data[0].quote}.</p>
    <footer class="blockquote-footer">${data[0].author} in <cite title="Source Title">${data[0].series}</cite></footer>
    </blockquote>`
    place_quote.html(text);
}
function getQuote(){
    fetch(api+`quote/random`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(reponse => reponse.json()).then(data => insertQuote(data));
}
//insert the data that we got from recherche
function insert_recherche_result(data, element_to_insert, key){
    if(data.length == 0){
        not_found = `<div class="row justify-content-md-center">
        <div class="col-md-2">
            <i class="fab fa-jsfiddle fa-10x"></i>
        </div>
        </div>
        <div class="row justify-content-md-center">
            <div class="col">
                <h1 class="display-4 message">${key} isn't here</h1>
            </div>
        </div>`;
        $(element_to_insert).empty();
        $(element_to_insert).append(not_found);
    }else{
        insertHtmlCharacter(data, element_to_insert)
    }
}
//request for a character named name
function getByCharacterByName(name){
    if(name == ""){
        $("#recherche_result").empty();
    }else{
        fetch(api+`characters?name=${name}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(reponse => reponse.json()).then(data => insert_recherche_result(data, "#recherche_result", name));
    }
}
$(document).ready(()=>{
    //load the data 
    load_data(getRandomInt(46));
    //setup the button event
    button.click(getQuote);
    recherche.keyup(function(){
        getByCharacterByName(recherche.val());
    });
    recherche.keydown(function(even){
        if(even.which == 13){
            console.log("entre");
        }
    })
});