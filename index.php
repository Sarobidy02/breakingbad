<!DOCTYPE html>
<html>
    <head>
        <title>Breaking Bad</title>
        <!-- Required meta tags -->
        <meta charset="UTF-8">
        <!-- make it responsive -->
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- my CSS -->
        <link href="fontawsome/css/all.css" rel="stylesheet">
        <link href="CSS/bootstrap.css" rel="stylesheet">
        <link href="CSS/CSS.css?version=51" type="text/css" rel="stylesheet">
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div id="entete" class="col">
                    <div class="row justify-content-md-center">
                        <div id="titre" class="col-md-4">
                            <h1 class="title">The Breaking Bad</h1>
                        </div>
                    </div>
                    <div class="row justify-content-md-center">
                        <div id="desc_recherche" class="col-md-4">
                            <p class="desc_rech">Get data on every character</p>
                        </div>
                    </div>
                    <div class="row justify-content-md-center">
                        <div class="col-md-6">
                            <form>
                                 <div class="input-group mb-3">
                                        <input id="recherche" name="recherche" type="text" class="form-control recherche" placeholder="character name" aria-label="Recipient's username" aria-describedby="button-addon2">
                                        <input type="hidden" name="action" value="recherche">
                                        <div class="input-group-append">
                                            <button class="btn btn-outline-secondary search_bouton" id="button-addon2">
                                                <svg class="bi bi-search" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M10.442 10.442a1 1 0 0 1 1.415 0l3.85 3.85a1 1 0 0 1-1.414 1.415l-3.85-3.85a1 1 0 0 1 0-1.415z"/>
                                                    <path fill-rule="evenodd" d="M6.5 12a5.5 5.5 0 1 0 0-11 5.5 5.5 0 0 0 0 11zM13 6.5a6.5 6.5 0 1 1-13 0 6.5 6.5 0 0 1 13 0z"/>
                                                </svg>
                                            </button>
                                        </div>
                                    </div>
                            </form>
                        </div>
                    </div>
                    <div class="row">
                        <div class= "col">
                            <div class="row justify-content-md-center">
                                <div id="recherche_result" class="col-md-11">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div id="menu" class="col">
                            <ul class="nav">
                                <li class="nav-item">
                                    <a class="nav-link active men" href="#">Home</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link men" href="#">Death</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link men" href="#">Episode</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div id="corps" class="col">
                    <div class="row justify-content-md-center">
                        <div id="contenue" class="col-md-11">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div id="pieds" class="col">
                    <div class="row justify-content-md-center">
                        <div id="content_pre_quote" class="col-md-2">
                            <div class="row justify-content-md-center">
                                <div id="place_b" class="col-md-1">
                                    <button type="button" class="btn btn-light quote">Quote!</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-md-center">
                        <div id="quote" class="col-md-5">
                            <p class="place_quote"></p>
                        </div>
                    </div>
                    <hr/>
                    <div class="row justify-content-md-center">
                        <div id="row_icon" class="row col-md-2">
                            <div class="col-sm"><a class="link" href="#"><i class="fab fa-twitter fa-2x"></i></a></div>
                            <div class="col-sm"><a class="link" href="#"><i class="fab fa-instagram fa-2x"></i></a></div>
                            <div class="col-sm"><a class="link" href="#"><i class="fab fa-facebook-f fa-2x"></i></a></div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <script src="JS/jquery-3.5.1.min.js"></script>
        <script src="JS/ajax.js"></script>
    </body>
</html>