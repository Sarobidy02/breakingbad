#The Breaking Bad
---
**Version 1.0.0**

Vous êtes fan de la serie culte breaking bad, vous allez adorer ce site.

- recherche de personnage
- generation de quotes aléatoire
- personnage aléatoire

Tout ça dans un ambiance interactif, sans recharger la page.

Les données étant chargées de l'api 
https://www.breakingbadapi.com/api/

---

## License & copyright
© Sarobidy RAPETERAMANA
